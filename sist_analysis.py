# author: Sally Madden
import sys,os,subprocess
import time
import logging
import string,random
import argparse
import math
import gzip 
import sistutility
from sistutility import StrSeq as StrWindower
from sistutility import FileSeq as FileWindower
from multiprocessing import Pool

logging.basicConfig(filename='analysis_log.' + os.path.basename(__file__)+'.log',level=logging.INFO)
logger = logging.getLogger("SIST Analysis")

def parse_config(): 
    sections = ['all','ANALYSIS'] 
    try: 
        filename,subsection = args.useconfig.split(',') 
        sections.append(subsection)
        setattr(args,'prefix',sections)
        for key,value in sistutility.readconfig(filename,sections):
            setattr(args,key,value)
    except Exception as e: 
        logger.error("Error calling config parser was {}".format(repr(e)))

def get_call_parameters():
    parse_config()
    return {'inputfile':args.inputfile, 
            'sistcode':args.sistcode,
            'transitionargs':args.transitionargs,
            'useconfig':args.useconfig, 
            'resultdirectory':args.resultdirectory, 
            'winlen':args.analysis_window_size, 
            'coverage':args.coverage, 
            'findends':args.findends, 
            'length':args.max_length, 
            'require_interim_file':args.require_interim_file, 
            'istempdir':args.tempdir, 
            'label_firstposition':args.label_firstposition, 
            'fastalength':args.fastalength,
            'remove_bases':args.remove_bases, 
            'slotcount':args.slotcount, 
            'prefix':args.tmp_prefix,} 

def run_process(segment): 
        cmd = '{code} {seq}'.format(code = segment['code'], 
                                    seq = segment['seq']) 
        segment['result'] = subprocess.check_output(cmd,shell = True) 
        return segment

class Sequence(object):
    def __init__(self,sequence,infile = False):
        if infile:
            self.__class__= FileSequence
        self.sequence = sequence

    @property 
    def seq_description(self):
        return '(first and last 15 Bases displayed) {} ... {}'.format(self.sequence[0:15],self.sequence[-15:])

    @property
    def seqstring(self):
        return self.sequence

    @property
    def length(self):
        return len(self.sequence)
    
    def splitter(self,**kwargs):
        return StrWindower(**kwargs)

class FileSequence(Sequence):
    @property
    def seq_description(self):
        return self.sequence

    @property
    def length(self): 
        try:
            return self._sl
        except:
            with open(self.sequence,'r') as fh: 
                self._sl = sum([len(x) for x in fh.read(1024).strip()])
        return self._sl
    
    def splitter(self,**kwargs):
        return FileWindower(**kwargs)

class SISTAnalysis(object):
    def __init__(self,**kwargs):
        self.intargs  = ['winlen','coverage','slotcount','remove_bases']
        self.start_time = time.time()
        self.slots = 1
        self.positions = {}
        self.showbases = True
        self.findends = False
        self.label_firstposition = 0
        self.clipends = 0
        self.require_interim_file = False
        self.input_is_file = False
        for key,value in kwargs.items():
            self.setattr(key , value)
        if self.require_interim_file:
            self.__class__= FileSA
            self.tempdir = self.istempdir 
        self.codetocall = '{} {}'.format(self.sistcode,self.transitionargs) 
        self.sequence = Sequence(self.inputfile,infile = self.input_is_file)
        self.startposition = self.winlen - self.step - 1 
        self.endposition = self.sequence.length + self.startposition 
        self.remove_bases = self.winlen - self.step
        logger.info(self.description)
    
    def run(self): 
        self.prep()
        self.run_analyses()
        self.formatresults()
        self.writeresult()
        self.cleanup()
        logger.info( "Time in analysis: {} seconds".format(time.time() -
                                                        self.start_time))
   
    @property
    def fastalength(self):
        try: 
            return self._fastalength
        except:
            return None
    
    @fastalength.setter
    def fastalength(self,value):
        self._fastalength = value

    @property
    def prefix(self):
        return self._prefix
    
    @prefix.setter
    def prefix(self,value):
        self._prefix = value
    
    def writeresult(self):
        resfile = '{}.sist.gz'.format(self.resultfile)
        try: 
            with gzip.open(resfile, 'wb') as fh: 
                for result in self.results(): 
                    fh.write(result + '\n') 
        except Exception as e:
            for result in self.results(): 
                logger.error(result)
    
    @property
    def outofseq(self):
        if not self.fastalength:
            return False
        if self.label_firstposition > self.fastalength:
            return True
        return False

    def results(self):
        if self.showbases: 
            base_position = 0
            result_pos = result_col_pos = 1
        else:
            result_pos = 0
            result_col_pos = 1
        for position, result_array in self.positions.items(): 
            result = '\t'.join(['{}'.format(self.wave(column,result_array)) 
                                for column in range(len(result_array[result_pos][result_col_pos]))]) 
            try:
                yield '\t'.join([str(self.label_firstposition), result_array[0][base_position], result])
            except:
                yield '\t'.join([str(self.label_firstposition),result])
            self.label_firstposition+=1
            if self.outofseq: 
                return 

    def wave(self,colnum,results):
        result = [c[1][colnum] for c in results]
        return sum([float(a) * float(b) for a,b in
                    zip(result,self.weightarray)])/sum(self.weightarray) 

    def setattr(self,field,value):
        if field in self.intargs:
            setattr(self,field,int(value))
        else:
            setattr(self,field,value)
    @property
    def length(self):
        try:
            return self._length
        except:
            self.length = self.sequence.length
    
    @length.setter
    def length(self,value):
        self._length = value
    
    @property
    def resultdirectory(self): 
        return self._resultdirectory

    @resultdirectory.setter
    def resultdirectory(self,resdir = None):
        if not resdir:
            return
        if not os.path.exists(resdir):
            try:
                os.makedirs(resdir)
            except Exception as e:
                logger.error('Cannot create result directory {}. Error was {}'.format(resdir,repr(e)))
        self._resultdirectory = resdir

    @property
    def description(self):
        return ('Analysis created with:\n'
                'sequence: {seq} \n'
                'of length: {length} \n'
                'starting at : {start} \n'
                'slot count: {slot}\n'
                'running analysis: {code}').format( 
                    seq = self.sequence.seq_description, 
                    length = self.sequence.length,
                    start = self.label_firstposition,
                    slot = self.slotcount,
                    code = self.codetocall)

    def cleanup(self):
        pass

    def prep(self): 
        pass

    @property
    def resultfile(self): 
        analysis_name = self.prefix
        try:
            return os.path.join(self.resultdirectory,'sist_result_{}_{}'.format(
                analysis_name,self.label_firstposition))
        except Exception as e:
            logger.error("No resultfile. Error was {}".format(repr(e)))


    def parse_result_line(self,line,addpos=0):
        try:
            position_result = line.split('\t')
            position = int(position_result[0]) + addpos
            base = position_result[1]
            values = position_result[2:]
            return (position,base,values)
        except:
            return(0,0,0)
    
    def inseq(self,position):
        start = self.startposition + self.remove_bases
        end = self.endposition - self.remove_bases
        return position > start and position < end + 1

    def formatresults(self):
        for window in self.resultmap:
            read = False
            for line in window['result'].splitlines():
                if 'Position' in line:
                    read = True
                    continue
                if not read:
                    continue
                position,base,values = self.parse_result_line(
                    line,window['start'])
                if self.inseq(position):
                    seqpos = position - self.startposition
                    try:
                        self.positions[seqpos].append((base,values))
                    except:
                        self.positions[seqpos] = [(base,values)]

    def run_analyses(self): 
        pool = Pool(processes=self.slotcount)
        self.resultmap = pool.map(run_process,self.segments)
        sorted(self.resultmap, key = lambda res: res['start'])
    
    @property
    def inputseq(self):
        return self.sequence.seqstring

    @property
    def splitter(self): 
        splitterargs = {'seq':self.inputseq,
                        'winlen':self.winlen, 
                        'step':self.step, 
                        'findends':self.findends,} 
        return self.sequence.splitter(**splitterargs)
    
    @property
    def segments(self): 
        try:
            return self._segments
        except: 
            splitter = self.splitter 
            self._segments = [{'start': (pos  * self.step) - self.step , 
                               'seq': seq, 'code': self.codetocall} 
                              for pos,seq in enumerate(splitter.windows)] 
        return self._segments

    @segments.setter
    def segments(self,segments):
        self._segments = segments
    
    @property 
    def weightarray(self): 
        try:
            return self._wa
        except:
            lw = range(int(math.ceil(self.coverage/2.0))) 
            rw = reversed(range(int(self.coverage/2))) 
            self._wa = [2**x for x in lw] + [2**x for x in rw] 
        return self._wa 

    @property 
    def step(self): 
        return self.winlen/self.coverage
    
class FileSA(SISTAnalysis): 
    def cleanup(self):
        self.delete_tempfiles()

    def prep(self): 
        self.write_to_tempfiles()
        self.prepare_segments()
    
    @property
    def tempdir(self):
        try:
            logger.info("tempdir is {}".format(self._tempdir)) 
            return self._tempdir
        except:
            self.tempdir = sistutility.unique_dir(os.getcwd(),'sistresults',3)
            logger.info("tempdir is {}".format(self._tempdir))
        return self._tempdir
        
    @tempdir.setter
    def tempdir(self,tempdir):
        logger.info('tempdir setting to {}'.format(tempdir))
        if os.path.exists(tempdir):
            self._tempdir = tempdir
        else:
            try:
                permission = 0755
                os.makedirs(tempdir,permission)
                self._tempdir = tempdir
            except Exception as e:
                logger.error('Cannot create temp directory. '
                             'Error was {}'.format(repr(e)))
                raise e

    @property
    def prefix(self):
        try:
            return self._prefix
        except:
            self._prefix = 'tsf_'
        return self._prefix
    
    def segment_filename(self,segment):
        start = str(segment['start']).replace('-','minus')
        return os.path.join(self.tempdir,'{prefix}_{start}'.format(
            prefix = self.prefix,
            start = start))
    
    def writetempfile(self,filename,text):
        try: 
            with open(filename,'wb') as fh: 
                return fh.write(text)
        except Exception as e:
            logger.error(repr(e))
            raise

    def write_to_tempfiles(self):
        fastaline = '>fasta_line\n'
        try: 
            return [self.writetempfile(filename = self.segment_filename(s),
                                       text = '{}{}'.format(fastaline,s['seq'])) 
                    for s in self.segments] 
        except Exception as e: 
            logger.error('Exception creating tmp file was {}'.format(repr(e)))
            raise
    
    def prepare_segment(self,segment):
        segment['seq'] = self.segment_filename(segment)
        c = segment['code'].split()
        return segment

    def prepare_segments(self):
        self.write_to_tempfiles()
        self.segments = [self.prepare_segment(segment) for segment in self.segments]
    
    def delete_tempfiles(self):
        try:
            for tmpfile in [os.path.join(self.tempdir,self.segment_filename(s)) 
                            for s in self.segments]:
                os.remove(tmpfile)
            os.rmdir(self.tempdir)
        except Exception as e:
            logger.error('Failed to remove tempfiles. Error: '
                         '{}'.format(repr(e)))

if __name__=='__main__':
    parser = sistutility.VerboseParser()
    parser.add_argument('-i','--inputfile',type = str,
                        help = 'input file ')
    parser.add_argument('-s','--sistcode',type = str,help = 'full path to sist master.pl')
    parser.add_argument('-e','--fastalength',type = int,help = 'length of original fastalength')
    parser.add_argument('-T','--transitionargs',type = str,help = 'arguments to send to sist code')
    parser.add_argument('-C','--useconfig',type = str,
                        help = 'use config file for arguments to this script.filename,section',
                        default = False)
    parser.add_argument('-d','--resultdirectory',type = str,
                        help = 'directory for results',default = False)
    parser.add_argument('-B','--resultsubdirectory',type = str,
                        help = 'directory for results',default = False)
    parser.add_argument('-w','--analysis_window_size',type = int,
                        help = 'window length',default = 10)
    parser.add_argument('-c','--coverage',type = int,
                        help = 'coverage for each base', default = 5)
    parser.add_argument('-f','--findends',action = 'store_true',
                        help = 'add ends (for linear seqs')
    parser.add_argument('-m','--max_length',type = int,
                        help = 'max len of result file')
    parser.add_argument('-q','--require_interim_file',action = 'store_true',
                        help = 'require interim file (true for ir finder')
    parser.add_argument('-n','--tempdir',type = str,
                        help = 'temp dir, req if require_temp_file is true')
    parser.add_argument('-L','--label_firstposition',type = int,default = 0,
                        help = 'Number to start position count')
    parser.add_argument('-r','--remove_bases',type = int,default = 0,
                        help = 'count bases to remove from ends before return')
    parser.add_argument('-S','--slotcount',type = int,default = 0,
                        help = 'number of processors (slots) to use ')
    parser.add_argument('-t', '--tmp_prefix',type = str, default = 'tsr_',
                        help = 'file prefix for temp files')
    parser.add_argument('-D', '--sist_dirname',type = str, default = 'sist_temp',
                        help = 'sist subdir for temp files')
    parser.add_argument('-l','--lengthofseq',type = str,default = None,
                        help = 'length of output required. Default:len input') 
    if len(sys.argv)==1: 
        parser.print_help() 
        sys.exit(1) 
    args = parser.parse_args()

    def main():
        params = get_call_parameters()
        analysis = SISTAnalysis(**params)
        analysis.run()

    main()
