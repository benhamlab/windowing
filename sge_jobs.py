import os
import random
import subprocess
import string

class Job(object):
    def __init__(self,cmd,slots=32, holdforids=None,extra=None, memory=None, project=None, jobname=None, runfolder=None):
        self.cmd = cmd
        self.waitstring = ''
        if holdforids: 
            self.waitstring = '-hold_jid {}'.format(','.join(str(sgeid) for sgeid in holdforids))
        self.extra=extra
        self.slots = slots
        self.memory = memory
        self.jobname = jobname
        self.project = project
        if runfolder is None:
            runfolder = os.path.join(os.path.expanduser("~"),"sgejobs")
            runfolder = os.path.join('/share/benham_working_data/data/','jobfolder')
        if not os.path.exists(runfolder):
            os.makedirs(runfolder)
        self.runfolder = runfolder
        self.sgeId = None

    def __makerunfolder(self):
        if self.jobname is None:
            self.jobname = 'j'+(''.join(random.choice('qwertyuiopasdfghjklzxcvbnm1234567890') for x in range(10)))

        pathtojob = os.path.join(self.runfolder, self.jobname)
        while os.path.exists(pathtojob):
            pathtojob+=random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
        os.makedirs(pathtojob)
        return pathtojob

    def run(self):
        if self.sgeId is not None: 
            return 
        runfolder = self.__makerunfolder() 
        script = os.path.join(runfolder, 'script.sh')

        options = []
        if self.slots is not None:
            options.append('-pe threaded %d'%self.slots)
        if self.memory is not None:
            options.append('-l s_vmem=%dG'%(self.memory-.2))
            options.append('-l h_vmem=%dG'%self.memory)
        if self.extra is not None:
            options.extend(self.extra)
        options.append('-o %s/stdout'%runfolder)
        options.append('-e %s/stderr'%runfolder)
        options.append('-N %s'%self.jobname)
        options.append('-S /bin/bash')
        if self.waitstring:
            options.append(self.waitstring)
        with open(script,'w') as scriptFile:
            scriptFile.write('unset module\n')
            for option in options:
                scriptFile.write('#$ %s\n'%option)
            jobObj = {'job_id':'${JOB_ID}', 'machine':'${HOSTNAME}', 'status':'started'}
            jobObj['exit_code'] = '$?'
            jobObj['status'] = 'Finished'
            scriptFile.write( 'cd %s \n'%runfolder)
            scriptFile.write( self.cmd+"\n")
        qsubcmd = 'qsub '
        if self.project is not None:
            qsubcmd += '-P %s '%self.project
        qsubcmd+=script + '\n'
        output = subprocess.check_output(qsubcmd, shell=True)
        self.sgeId = int(output.split('Your job ')[1].split(' ')[0])
        return self.sgeId
