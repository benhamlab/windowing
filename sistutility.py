import logging
import os
import random
import string
from datetime import datetime
import argparse
import sys
import errno

class VerboseParser(argparse.ArgumentParser): 
    def error(self, message): 
        sys.stderr.write('error: {}\n'.format(message)) 
        self.print_help() 
        sys.stderr.write('error: {}\n'.format(message))
        sys.exit(2)

def clean_value(s):
    s = s.strip()
    try:
        return int(s)
    except ValueError:
        return s

def makedirectory(dirname): 
    try: 
        os.mkdir(dirname) 
    except OSError as e: 
        if e.errno != errno.EEXIST: 
            raise e 
        pass


def nowtostr(): 
    format = ('%H%m%f_%Y%m%d')
    now = datetime.now()
    return now.strftime(format)
        
def unique_dir(path,prefix,suffix = '',add = True,userandom = True,length = 8): 
    if add:
        if userandom:
                suffix = ''.join([random.choice(string.letters)for s in range(length)])
        else:
                suffix = nowtostr()
    else: 
        add = ''
    name = '{prefix}_{suffix}'.format(prefix = prefix,suffix = suffix)
    udir = os.path.join(path,name) 
    if not add:
        return udir
    if os.path.exists(udir):
        unique_dir(path = path, prefix = prefix, userandom = userandom, length= length)
    return udir

def useline(line):
    line = line.strip()
    if not line or line.startswith('#'):
        return False
    return line

def readconfig(configfile,configsections = ['all']):
    configsections+=['all']
    read = False
    try: 
        with open(configfile,'rb') as config:
            lines =[line.strip() for line in config if useline(line)]
            for line in lines: 
                if '[' in line: 
                    read = False 
                    if ''.join(line[1:-1]) in configsections: 
                        read = True 
                        continue 
                if read: 
                    key,value = line.split('=')
                    result = (key.strip(),clean_value(value)) 
                    yield result
    except Exception as e: 
        print(repr(e))
        raise e 

def configvalue(configfile,sections,field):
    try:
        for result in readconfig(configfile,sections):
            if field == result[0]:
                return result[1]
    except Exception as e: 
        print(repr(e))
        
        
class SplitSeq(object):
    def __init__(self,**kwargs):
        for key,value in kwargs.items():
            setattr(self, key, value)

    @property
    def endslength(self):
        try:
            return self._endslength
        except:
            self._endslength = self.winlen - 1
        return self._endslength
    
    @endslength.setter
    def endslength(self,value):
        self._endslength = value

    def get_random_seq(self):
        return ''.join([random.choice(['A','T','G','C']) for x in
                        range(self.endslength)])
    
    def isbase(self,char):
        bases = 'ATGCN'
        ch = char.strip()
        try:
            if ch.upper() in string.letters:
                if ch.upper() in bases: 
                    return ch
                return 'N'
            return ''
        except:
            return ''
    
    def line(self,line): 
        if line.startswith('>'): 
            try: 
                line = line[line.index('\n'):] 
            except: 
                line = line
        try:
            return ''.join([self.isbase(x) for x in line])
        except Exception as e:
            print repr(e)
    
    
    @property 
    def windows(self):
        window = self.start
        while True:
            line = self.nextline
            if line:
                window+=line
                if len(window) > self.winlen:
                    yield window[0:self.winlen]
                    window = window[self.step:]
            else:
                window+=self.end
                while window:
                    yield window[0:self.winlen]
                    window = window[self.step:]
                return
    @property
    def sequencelength(self):
        try: 
            return self._sequencelength
        except:
            return None
                
class FileSeq(SplitSeq):
    @property
    def sequencelength(self):
        try:
            return self._sequencelength
        except:
            chars = 0 
            for line in self.fh: 
                chars += len(line.strip()) 
            self._sequencelength = chars 
            self.fh.close()
            self._fh = None
        return self._sequencelength

    def removefastaline(self):
        firstline = self.fh.readline()
        if not firstline.startswith('>'):
                self.fh.seek(0,0)

    @property
    def nextline(self): 
        return self.line(self.fh.read(self.winlen))
    
    @property
    def fh(self):
        try:
            return self._fh
        except:
            try:
                self._fh = open(self.seq,'rb')
                self.removefastaline()
            except:
                print 'bad'
        return self._fh
    
    @property
    def start(self):
        try:
            return self._start
        except:
            if self.findends:
                ret = ''
                for chunk in self.fh.read(self.winlen).strip():
                    ret+=chunk
                    if len(ret) > self.endslength:
                        ret = ret[-self.endslength:]
                self.fh.seek(0,0)
                self._start = ''.join(reversed(ret[-self.endslength:]))
            else:
                self._start = self.get_random_seq()
            return self._start
    
    @property
    def end(self):
        try:
            return self._end
        except:
            if self.findends: 
                self.fh.seek(0,0)
                ret = ''
                for chunk in self.fh.read(self.winlen).strip():
                    ret+=chunk
                    if len(ret) > self.endslength:
                        self._end = ret[0:self.winlen]
            else:
                self._end = self.get_random_seq() 
            return self._end

class StrSeq(SplitSeq):
    @property
    def nextline(self):
        try:
            self.pos+=1
        except:
            self.pos=0
        start = self.pos * self.winlen
        return self.line(self.seq[start:start+self.winlen])
    
    @property
    def start(self):
        try:
            return self._start
        except:
            if self.findends:
                self._start = reversed(self.seq[-self.winlen:])
            else:
                self._start = self.get_random_seq()
        return self._start
    
    @property
    def end(self): 
        try:
            return self._end
        except:
            if self.findends: 
                self._end = self.seq[0:self.winlen -1]
            else:
                self._end = self.get_random_seq()
        return self._end
        

