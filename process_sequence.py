# filename:process_sequence.py
# author: Sally Madden

import sys,os,subprocess
import logging
import string,random
import argparse
from sge_job import Job
import sistutility
from sistutility import FileSeq,StrSeq
import shutil

logging.basicConfig(filename='mainprocessor.' + os.path.basename(__file__)+'.log',level=logging.INFO)
logger = logging.getLogger("SIST Analysis")

def send_job(cmd,slots = 32,holdlist = None,dist = True,out = None,err = None ,log = None):
    location = '{}/'.format(os.path.dirname(os.path.realpath(__file__)))
    jobname = ''.join([cmd[:4],''.join(random.choice(string.letters) for i in range(10))])
    jobcmd = 'python {loc}{cmd}'.format(
            loc = location,
            cmd = cmd)
    if not dist:
        subprocess.call(jobcmd,shell = True)
        return None
    logger.info("sending command {}".format(jobcmd))
    if not holdlist:
        holdlist = []
    return Job(jobcmd,slots = slots, jobname = jobname, holdforids = holdlist).run()

def splitter():
    splitterargs = {'seq':__fastafile, 
                    'winlen':__processing_segment_size, 
                    'step':__processing_window_size, 
                    'endslength':__padding_size , 
                    'findends':__circular,}
    return FileSeq(**splitterargs)

def segments(): 
    for segment in splitter().windows:
        yield segment
           
def writecmd(cmd,analysis):
    if analysis == 'COMPETITION':
        cmd += ' -p comp_  -q -n {}'.format(__tempdir)
    return cmd

def run_analyses_segment(segment, starting_baseposition,fastalength,jobids):
    for analysis,prefix in __analyses.items(): 
        analysis_config = '{},{}'.format(__sist_config_file,analysis) 
        cmd = 'sist_analysis.py -d {resultdir} -e {fastalength} -i {segment} -L {label_firstposition} -C {config}'.format(
                        segment = segment, 
                        label_firstposition = starting_baseposition, 
                        resultdir = __tempdir,
                        fastalength = fastalength,
                        config = analysis_config) 
        cmd = writecmd(cmd,analysis)
        jid = send_job(cmd)
        try:
            jobids[prefix].append(jid)
        except:
            jobids[prefix] = [jid]
    return jobids

def do_merge(jobids):
    for analysis,prefix in __analyses.items():
        section = [analysis]
        suff  = sistutility.configvalue(__sist_config_file,section,'tmp_prefix')
        fileprefix = '{}{}'.format(configval('tmp_prefix'), suff).replace('\'','')
        mergecmd = 'merge.py -p {prefix} -i {inputd} -r {resultdir}'.format(
                prefix = fileprefix,
                inputd = __tempdir,
                resultdir = '{}/{}resultfile'.format(__resultdir,fileprefix))
        logger.info("MERGEsending: {}".format(mergecmd)) 
        jobids[prefix] = (send_job(cmd = mergecmd,holdlist = jobids[prefix]))
    return jobids

def cleanup(jobids): 
    jobids = [jobid for prefix, jobid in jobids.items()]
    cmd = 'sist_cleanup.py -i {}'.format(__tempdir)
    logger.info("Would be sending: {}{}".format(cmd,jobids))
    send_job(cmd,holdlist = jobids)


def process_sequence(): 
    jobids = {}
    posinseq = 1
    totalpadding = 2 * __padding_size
    fastalength = splitter().sequencelength
    for segment in segments():
        if len(segment) >= totalpadding:
            jobids = (run_analyses_segment(
                        jobids = jobids,
                        segment = segment, 
                        starting_baseposition = posinseq,
                        fastalength = fastalength)) 
            posinseq+=__processing_window_size
    jobids = do_merge(jobids)
    cleanup(jobids)
        
def configval(field,addsec = None):
    sections = ['PROCESSING','ANALYSIS'] 
    if addsec:
        sections.append(addsec)
    print "sist file si {}".format(__sist_config_file)
    try:
        return sistutility.configvalue(__sist_config_file,sections,field)
    except Exception as e:
        logger.error('Search for {} in config file threw {}'.format(field,repr(e)))
        raise 

def analyses():
    return {analysis:sistutility.configvalue(__sist_config_file,[analysis],'prefix') for 
            analysis in [a.upper() for a in args.analyses.split(',')]}



if __name__ == "__main__": 
    parser = sistutility.VerboseParser()
    parser.add_argument('-i','--inputfile',type = str, help = 'input file ')
    parser.add_argument('-c','--sistconfig',type = str,help = 'full path sist_config')
    parser.add_argument('-s','--shapecircular',action = 'store_true',help = 'circular')
    parser.add_argument('-a','--analyses',type = str,help = 'Analyses to perform. ' 
                        'Use comma delimited list of headings from config file') 
    if len(sys.argv)==1: 
        parser.print_help() 
        sys.exit(1) 
    args = parser.parse_args()
    if os.access(args.sistconfig,os.R_OK):
        __sist_config_file = args.sistconfig
    if os.access(args.inputfile,os.R_OK):
        __fastafile = args.inputfile
    __circular = args.shapecircular
    __analyses = analyses()
    __analysis_window_size = configval('analysis_window_size')
    __processing_window_size = configval('processing_window_size') 
    coverage = configval('coverage') 
    try: 
        __stride = (__analysis_window_size/coverage)
    except ZeroDivisionError:
        print 'Coverage not found in config file'
        raise z 
    __padding_size = __analysis_window_size - __stride  
    __processing_segment_size =  __processing_window_size + (2 * __padding_size) 
    #set up result and temp dirs 
    fastafilename = os.path.basename(__fastafile).replace('.','_')
    __resultdir = sistutility.unique_dir(path = configval('resultdirectory'),
            prefix = fastafilename,
            suffix = 'result_file',
            add = False,
            userandom = False)
    __tempdir = sistutility.unique_dir(path = configval('tempdirectory'),
            prefix = 'sist_tmp_',
            suffix = fastafilename) 
    sistutility.makedirectory(__resultdir) 
    sistutility.makedirectory(__tempdir)
    print "Result will be available in {}".format(__resultdir)
    process_sequence()
