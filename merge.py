import os
import sys
import argparse
import gzip
import glob
import sistutility
import shutil
import subprocess

def merge(resultfiles):
    resultfile = args.resultfile + '.gz'
    with gzip.open(resultfile, 'wb') as wfh:
        for fname in resultfiles:
            with gzip.open(fname,'rb') as rfh:
                wfh.writelines(rfh)

def getkey(filename): 
    return int(filename.split(args.suffix)[0].split('_')[-1])
        
def main():
    results = '{}/{}*'.format(args.inputdir,args.prefix)
    merge(sorted(glob.glob(results), key = getkey))
    cleanup = [os.remove(f) for f in results]

if __name__=='__main__': 
    parser = sistutility.VerboseParser() 
    parser.add_argument('-i','--inputdir',type = str, help = 'input dir')
    parser.add_argument('-s','--suffix',type = str, help = 'file suffix',
            default = '.sist.gz')
    parser.add_argument('-p','--prefix',type = str,
                        help = 'file prefix')
    parser.add_argument('-r','--resultfile',type = str,
                        help = 'directory for results',default = False)
    parser.add_argument('-n','--not_compressed',action = 'store_true', help = 'input and output not compressed')
    args = parser.parse_args()
main()

